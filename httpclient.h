#ifndef HTTPCLIENT_H
#define HTTPCLIENT_H

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

#include "qobject.h"


class HttpClient : public QObject
{
    Q_OBJECT

public:
    explicit HttpClient(QObject* parent = nullptr);


public slots:

    void get(const QString& endpoint);
    void post(const QString& endpoint, const QByteArray& data);

private slots:
    void readyRead();

private:
    void handleStatusCode(int statusCode);
    void success();
    void notFound();
    void internalServerError();
    void unknownStatusCode(int statusCode);

    void processResponse(const QByteArray& response);


    QNetworkAccessManager manager;
};

#endif // HTTPCLIENT_H
