#include "httpclient.h"


HttpClient::HttpClient(QObject* parent) : QObject(parent)
{
}


void HttpClient::get(const QString& endpoint)
{
    QNetworkReply* reply = manager.get(QNetworkRequest(QUrl(endpoint)));
    connect(reply, &QNetworkReply::readyRead, this, &HttpClient::readyRead);
}


void HttpClient::post(const QString& endpoint, const QByteArray& data)
{
    QNetworkRequest request = QNetworkRequest(QUrl(endpoint));
    request.setHeader(QNetworkRequest::ContentTypeHeader, "text/plain");

    QNetworkReply* reply = manager.post(request, data);
    connect(reply, &QNetworkReply::readyRead, this, &HttpClient::readyRead);
}


void HttpClient::readyRead()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());

    if (reply) {

        if (reply->error() == QNetworkReply::NoError) {
            processResponse(reply->readAll());

        } else {
            int statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

            handleStatusCode(statusCode);
        }

        reply->deleteLater();
    }
}


void HttpClient::processResponse(const QByteArray& response)
{
    Q_UNUSED(response);
    qInfo() << "Processing response...";
}


void HttpClient::handleStatusCode(int statusCode)
{
    switch (statusCode) {

    case 200:
        success();
        break;

    case 404:
        notFound();
        break;

    case 500:
        internalServerError();
        break;

    default:
        unknownStatusCode(statusCode);
        break;
    }
}


void HttpClient::success()
{
    qInfo() << "Successful response (Status code: 200)";
}


void HttpClient::notFound()
{
    qInfo() << "Resource not found (Status code: 404)";
}


void HttpClient::internalServerError()
{
    qInfo() << "Internal server error (Status code: 500)";
}


void HttpClient::unknownStatusCode(int statusCode)
{
    qInfo() << QString("Unknown status code: %1").arg(statusCode);
}
