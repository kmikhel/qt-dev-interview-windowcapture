#include <QCoreApplication>

#include "httpclient.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    HttpClient httpClient;

    QByteArray data;
    data.append("param1=hello");
    data.append("&");
    data.append("param2=foo");

    httpClient.post("https://postman-echo.com/post", data);

    return a.exec();
}
